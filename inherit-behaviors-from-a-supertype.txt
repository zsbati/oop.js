function Animal() { }

Animal.prototype = {
  constructor: Animal,
  eat: function() {
    console.log("nom nom nom");
  }
};

// Only change code below this line
/*Use Object.create to make two instances of Animal named duck and beagle.*/

let duck = Object.create(Animal.prototype); // Change this
let beagle = Object.create(Animal.prototype); // Change
