function Dog(name) {
  this.name = name;
}

// Only change code below this line
/*Write a joinDogFraternity function that takes a candidate parameter and, 
using the constructor property, return true if the candidate is a Dog, 
otherwise return false. Since the constructor property can be overwritten 
(which will be covered in the next two challenges) 
it’s generally better to use the instanceof method to check the type of 
an object.*/
function joinDogFraternity(candidate) {
if (candidate.constructor === Dog) {
    return true;
  } else {
    return false;
  }
}
